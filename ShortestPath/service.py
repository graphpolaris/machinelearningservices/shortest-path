##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

#!/usr/bin/env python
import os
import sys
import inspect
import networkx as nx
import json

from machine_learning_common.MLBasicFunctions import (
    addNodeMetaData,
    buildGraph,
    optionalCheck,
)
from machine_learning_common.MLRepositoryInterface import MLServerInterface

# We often compare against this specific string, so we clearly define it to prevent any typos
# Python has no constants so we simply give it a very obvious name that implies it is not supposed to be changed
CONST_SOURCE = "srcNode"
CONST_TARGET = "trtNode"
ML_QUEUE_NAME = "stp_queue"
SERVICE_NAME = "shortestPath"


##
# MLServer implements the MLServerInterface interface
##
class MLServer(MLServerInterface):
    ##
    # __init__ initializes the MLServer with the proper parameters
    #   self: Self@MLServer, the MLServer implementation
    #   Return: None, return nothing
    ##
    def __init__(self):
        # Fill in the parameters for communication with the algorithm here
        super().__init__(SERVICE_NAME, ML_QUEUE_NAME)
        self.parameters["source"] = self.Attribute(True, "Node")
        self.parameters["target"] = self.Attribute(True, "Node")

    ##
    # decodeMessage builds a NetworkX graph based on the incoming query data
    #   self: Self@MLServer, the MLServer implementation
    #   incomingQueryData: Any, the incoming query data in JSON format
    #   Return: Graph, the NetworkX graph
    ##
    def decodeMessage(self, incomingQueryData):
        params = {}
        if "mlAttributes" not in incomingQueryData:
            raise Exception("No mlAttributes provided")
        if len(incomingQueryData["mlAttributes"]) != 2:
            raise Exception("Exactly two mlAttributes are required for shortestPath")
        params[CONST_SOURCE] = incomingQueryData["mlAttributes"][0]
        params[CONST_TARGET] = incomingQueryData["mlAttributes"][1]
        graph = buildGraph(incomingQueryData)
        return (params, graph)

    ##
    # adjustData adjusts the machine learning results into a format we can work with
    #   self: Self@MLServer, the MLServer implementation
    #   mlResult: Any, the result produced by the machine learning algorithm
    #   srcNode: Any, the sourcenode
    #   trtNode: Any, the targetnode
    #   Return: dict, a dict matching all shortest paths to their respective nodes
    ##
    def adjustData(self, mlResult, srcNode, trtNode):
        dict = {}
        if srcNode != None:
            if trtNode != None:
                dict[srcNode] = {}
                dict[srcNode][trtNode] = mlResult
            else:
                dict[srcNode] = mlResult
        else:
            if trtNode != None:
                for i in mlResult:
                    dict[i] = {}
                    dict[i][trtNode] = mlResult[i]
            else:
                dict = mlResult
        return dict

    ##
    # __call__ takes an incoming message and applies the machine learning algorithm and data transformations
    #   self: Self@MLServer, the MLServer implementation
    #   body: Any, the body of the incoming RabbitMQ message
    #   Return: str, a formatted JSON string of the query result after the application of a machine learning algorithm
    ##
    def __call__(self, body):
        # Turn JSON into a dictionary
        incomingQueryData = json.loads(body.decode())
        print(incomingQueryData["queryID"])
        print(incomingQueryData["type"])
        # Use the query-result to build a graph and return a dictionary of parameters
        (pr, G) = self.decodeMessage(incomingQueryData)
        # Optional input parameters get parsed to None
        prparsed = optionalCheck(pr)
        # Get the machine learning results
        mlresult = nx.shortest_path(G, prparsed[CONST_SOURCE], prparsed[CONST_TARGET])
        # Standardize the output format for this specific algorithm
        data = self.adjustData(mlresult, prparsed[CONST_SOURCE], prparsed[CONST_TARGET])

        return data
