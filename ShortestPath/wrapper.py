##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

#!/usr/bin/env python

import argparse
import inspect
from pathlib import Path
from dotenv import load_dotenv
import sys
import os

parser = argparse.ArgumentParser(description="Machine Learning Service")
parser.add_argument("--prod", type=bool, default=False)
args = parser.parse_args()

if args.prod:
    # load_dotenv("../.env.dev")
    pass
else:
    load_dotenv("../.env.local")

    # Importing in Python is rather complicated when attempting to adhere to clean code architecture
    # These few lines set the import paths so Docker can actually build the images without having to resort to copy paste-ing
    currentdir = Path(
        os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    )
    sys.path.append(str(currentdir.parent.parent / "machine-learning-common"))


from service import MLServer
from machine_learning_common.MLDriver import MLDriver


##
# __main__ starts the MLServer and listens for incoming messages
#   Return: None, returns nothing
##
def __main__():
    server = MLServer()
    MLDriver(server).listen()


__main__()
